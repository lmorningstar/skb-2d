import express from 'express';
import cors from 'cors';
import convert from 'color-convert';

const app = express();

app.use(cors());

/*
function rgbToHex(r, g, b) {
  let rH = r.toString(16);
  let gH = g.toString(16);
  let bH = b.toString(16);

  if (rH.length === 1) rH = `0${rH}`;
  if (gH.length === 1) gH = `0${gH}`;
  if (bH.length === 1) bH = `0${bH}`;

  return (rH + gH + bH);
}
*/

app.get('/colors', (req, res) => {
  let { color } = req.query;
  console.log(color);
  let normalColor = '';
  if (color) {
    color = color.trim();
    if (color.search(/hsl/g) !== -1 && color.search(/#/) === -1) {
      if (color.search(/%/) === -1) {
        console.log('Invalid color');
        res.send('Invalid color');
        return;
      }
      color = color.replace(/%20/g, '');
      color = color.replace(/[hsl()%]/gi, '');
      const colorArr = color.split(/,/);
      if (colorArr.length !== 3) {
        console.log('Invalid color');
        res.send('Invalid color');
        return;
      }
      if (+colorArr[1] > 100 || +colorArr[1] < 0 || +colorArr[2] > 100 || +colorArr[2] < 0) {
        console.log('Invalid color');
        res.send('Invalid color');
        return;
      }
      if (+colorArr[1] > 0 && +colorArr[2] === 0) {
        console.log('Invalid color');
        res.send('Invalid color');
        return;
      }
      console.log(colorArr[0].trim(), colorArr[1].trim(), colorArr[2].trim());
      color = convert.hsl.hex(+colorArr[0].trim(), +colorArr[1].trim(), +colorArr[2].trim());
    }
    if (color.search(/rgb/g) !== -1 && color.search(/#/) === -1) {
      color = color.replace(/[rgb()]/gi, '');
      const colorArr = color.split(/,/);
      if (colorArr.length !== 3) {
        console.log('Invalid color');
        res.send('Invalid color');
        return;
      }
      if (+colorArr[0] > 255 || +colorArr[1] > 255 || +colorArr[2] > 255) {
        console.log('Invalid color');
        res.send('Invalid color');
        return;
      }
      console.log(colorArr[0].trim(), colorArr[1].trim(), colorArr[2].trim());
      color = convert.rgb.hex(+colorArr[0].trim(), +colorArr[1].trim(), +colorArr[2].trim());
      //color = rgbToHex(+colorArr[0].trim(), +colorArr[1].trim(), +colorArr[2].trim());
    }
    color = color.replace(/#/, '');
    console.log(color);
    if (color.search(/[g-zG-Z-]/) === -1 && (color.length === 3 || color.length === 6)) {
      if (color.length === 3) {
        normalColor = `${color[0]}${color[0]}${color[1]}${color[1]}${color[2]}${color[2]}`;
      } else {
        normalColor = color;
      }
      normalColor = normalColor.toLowerCase();

      console.log(normalColor);
      res.send(`#${normalColor}`);
      return;
    }
  }

  console.log('Invalid color');
  res.send('Invalid color');
  return;
});

app.listen(3000, () => {
  console.log('App start');
});
